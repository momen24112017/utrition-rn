// That page is responsible for navigation setup 
import * as React from 'react';
//Navogation libraries
import {NavigationContainer} from '@react-navigation/native';
import DrawerNavigator from './src/Navigation/DrawerNavigator';
import {Provider} from 'react-redux';
import {createStore , applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import SplashScreen from 'react-native-splash-screen'

import reducer from './src/reducers';

const store =createStore(reducer, applyMiddleware(thunk));



export default class App extends React.Component {
    componentDidMount() {
        SplashScreen.hide();
    }

    render() {

        return (
            <Provider store={store}>
                <NavigationContainer>             
                <DrawerNavigator />
                </NavigationContainer>
            </Provider>
        )
    }
}


