import * as types from './actionTypes'

import {ContactUsURL, ListPlansURL, LoginURL, RegisterURL} from '../constants/api';

import { Alert } from 'react-native'; // to show alerts in app
import axios from 'axios';

export const listPlans =() => async dispatch => {
   const response = await axios.get(`${ListPlansURL}`);
    
   dispatch({ type : 'LIST_PLAN' , payload : response.data});
}

const setLoginState = (loginData) => {
   return {
     type: types.SET_LOGIN_STATE,
     payload: loginData,
   };
 };

 const setRegisterState = (registerData) => {
   return {
     type: types.SET_REGISTER_STATE,
     payload: registerData,
   };
 };

 const setContactState = (contactData) => {
  return {
    type: types.SET_CONTACT_STATE,
    payload: contactData,
    isSent: true

  };
};



 export const login = (email, password) => {
  
   return (dispatch) => {  // don't forget to use dispatch here!
      return  axios.post(LoginURL,{
         email,
         password,
         
     }).then(response=>{
      console.log(response.data.status.code );
        if (response.data.status.code === 200) { 
          
         dispatch(setLoginState({ ...response, userId: email })); 
         Alert.alert('Login Sucess', 'Hello to our app');
        return true;
         
       } else {
           Alert.alert('Login Failed', 'Email or Password is incorrect');
           return false;
       } 
     }).catch(error =>{
      Alert.alert('Login Failed', 'Some error occured, please retry');
      console.log(error);
      return false;
         
     });

 
  };
};



export const register = (name ,email, password , age , phone , address) => {
  
   return (dispatch) => {  // don't forget to use dispatch here!
      return  axios.post(RegisterURL,{
         name,
         email,
         password,
         age,
         phone,
         address
         
     }).then(response=>{
        if (response.data.status.code === 200) { 
          
         dispatch(setRegisterState({ ...response, userId: email })); 
         
         Alert.alert('Register Sucess', 'Hello to our app');
         return true;
         
       } else {
           Alert.alert('Register Failed', 'Some data is incorrect');
           return false;
       } 
     }).catch(error =>{
      Alert.alert('Register Failed', 'Some error occured, please retry');
      return false;
         
     });

 
  };
};

export const contactUs = (first_name ,last_name, email , phone, message) => {
 
  return (dispatch) => {  // don't forget to use dispatch here!
     return  axios.post(ContactUsURL,{
       first_name ,
        last_name,
        email,
        phone ,
        message ,
   
        
    }).then(response=>{
       if (response.data.status.code === 200) { 
         
        dispatch(setContactState({ ...response, userId: email })); 
        Alert.alert('Sucess', 'Your Message Sent');
        return true;
      } else {
          Alert.alert('Failed', 'Some data is incorrect');
          return false;
      } 
      // console.log(response.data);
    }).catch(error =>{
     Alert.alert('Failed', 'Some error occured, please retry');
     return false;
        
    });


 };
};

