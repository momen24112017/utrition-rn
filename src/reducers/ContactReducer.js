import * as types from '../actions/actionTypes';

export default  contactReducer = (state = [], action) => {
    switch (action.type) {
      case types.SET_CONTACT_STATE:
        return {
          ...state,
          ...action.payload, // this is what we expect to get back from API call and login page input
          ...action.isSent
        };
      default:
        return state;
    }
  };