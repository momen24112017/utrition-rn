import {combineReducers} from 'redux';
import AuthReducer from './AuthReducer';
import listPlans from './ListPlans';
import {loginReducer , registerReducer} from './AuthReducer';
import contactReducer from './ContactReducer'

export default combineReducers({
    plans : listPlans,
    loginReducer: loginReducer,
    registerReducer,
    contactReducer
});