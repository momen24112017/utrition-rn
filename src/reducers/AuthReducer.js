  
import * as types from '../actions/actionTypes';

export const initialState = {
    isLoggedIn: false,
    userId: '',
    token: '',
    refreshToken: '',
    expiresOn: '',
    data: '',
  };


export const loginReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_LOGIN_STATE:
      return {
        ...state,
        ...action.payload, // this is what we expect to get back from API call and login page input
        isLoggedIn: true, // we set this as true on login
      };
    default:
      return state;
  }
};

export const registerReducer = (state = [], action) => {
    switch (action.type) {
      case types.SET_REGISTER_STATE:
        return {
          ...state,
          ...action.payload, // this is what we expect to get back from API call and login page input
        
        };
      default:
        return state;
    }
  };