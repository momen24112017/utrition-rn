import { Button, Overlay } from 'react-native-elements';
import { PHONE_HEIGHT, colors } from './styles';
import React, { useState } from 'react';
import {StyleSheet, Text, View} from 'react-native';

const Message = props => {
  

   return (
    <View>
 

      <Overlay 
        isVisible={props.visibility} 
        onBackdropPress={props.onBackdropPress} 
        // overlayStyle={styles.overlayContainer}
        
      >
        <View style={styles.overlayContainer}>
      <Text style={styles.text}>{props.text}</Text>
      {/* <Button
        title="OK"
        buttonStyle={styles.button}
        type="outline"
        buttonStyle={styles.button}
        titleStyle={styles.buttonText}
      /> */}
      </View>
      </Overlay>
    </View>
  );
};


const styles =StyleSheet.create({
  overlayContainer:
  {
    margin:20,
    // height:PHONE_HEIGHT *0.2,
    flexDirection: 'column',
    justifyContent: 'space-between'
  },
  text:
  {
    fontSize:16
  },
  button:
  {
   borderColor:colors.mainColor,
    borderRadius:PHONE_HEIGHT *0.05
  },
  buttonText:
  {
    color: colors.mainColor
  }

})
export default Message;
