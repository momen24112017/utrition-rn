import { Avatar, Overlay, Text } from 'react-native-elements';
import { ImageBackground, StyleSheet, TouchableOpacity, View } from 'react-native';
import {PHONE_HEIGHT, PHONE_WIDTH, colors} from './styles';
import React, { useState } from 'react';

const Story = props =>{
    const [visible, setVisible] = useState(false);

    const toggleOverlay = () => {
      setVisible(!visible);
    }
    return (
        <View>
        <TouchableOpacity
        onPress={toggleOverlay}
        >
        <Avatar

            rounded
            size="large"
            source={props.image}
            avatarStyle={styles.avatar}
        />
        </TouchableOpacity>
     
     
     
     
        <Overlay
         isVisible={visible}
         onBackdropPress={toggleOverlay}
        //  style
         overlayStyle={styles.overlayContainer}
>
             <View style={styles.overlayContentContainer}>
            
             <ImageBackground style={styles.tileImage} source={props.image}>
                
                <TouchableOpacity style={styles.exitButtonContainer} onPress={toggleOverlay}>
                    <Text style={styles.exitButton}>X</Text>
                </TouchableOpacity>

                <View style={styles.textContainer}>
                    <Text style={styles.header} h3>{props.header}</Text>
                    <Text style={styles.contnet}>{props.content}</Text>
                </View>
        </ImageBackground>

              
                
                    </View>
        </Overlay>
      </View>
    )
}

const styles = StyleSheet.create({
    avatar : {
        margin:5 ,
        borderWidth:2 ,
        borderRadius:50 ,
        borderColor :colors.mainColor,
        
    },
    overlayContainer:
    {
       
        width:PHONE_WIDTH ,
        backgroundColor: colors.transparentDarkHeavey,
        height: PHONE_HEIGHT
        
    },
    overlayContentContainer:
    {
        flex:1,
        
        height:PHONE_HEIGHT , 
       
    },
    tileImage:
    {
        
        height:PHONE_HEIGHT , 
    },
    exitButton:
    {
        backgroundColor: colors.transparentDarkLight,
        borderRadius: 50,
        alignSelf:'flex-end',
        color:colors.textLight,
        margin:20,
        fontSize:15,
        fontWeight:'bold',
        padding:5,
        paddingHorizontal:10
    },
    textContainer:
    {
     justifyContent:'center',
     alignItems:'center',
     flex:1   
    },

    header:
    {
        backgroundColor: colors.transparentDarkLight,
        color:colors.textLight,
        textAlign:'center',
        margin:10
    },
    contnet:
    {
        backgroundColor: colors.transparentDarkLight,
        color:colors.textLight,
        margin:30,
        textAlign:'center',
        padding:5,
        fontSize:15
    }
    
})
export default Story;