import { Dimensions, StyleSheet } from 'react-native';

export const PHONE_WIDTH = Dimensions.get('window').width;
export const PHONE_HEIGHT = Dimensions.get('window').height;

export const colors ={
    mainColor : '#7eb004',
    textDark: '#000',
    textLight: '#fff',
    textGray: 'gray',
    textOrange:'orange',
    blue :'blue',
    red :'red',
    transparentDarkHeavey: 'rgba(0,0,0,0.9)',
    transparentDarkMedium : 'rgba(0,0,0,0.6)',
    transparentDarkLight : 'rgba(0,0,0,0.2)',
    
    

}

