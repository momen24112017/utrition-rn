import React from 'react';
import {Text} from 'react-native';
import { colors } from './styles';

export default ErrorText =props=>{
    return <Text style={{alignSelf:'center' , color: colors.red, fontSize:15 , marginVertical:5}}>{props.message}</Text>
}