import React from 'react';
import {  StyleSheet , View} from 'react-native';
import { Avatar , Card ,Image ,Text , Button} from 'react-native-elements';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {colors, PHONE_WIDTH} from './styles';



const DataCard = (props) =>{
    
    return (
        <Card containerStyle={{padding: 0 , minWidth: PHONE_WIDTH * 0.6 , width: PHONE_WIDTH * 0.7 }}>
 
   
        
          <Image
            style={{ height:200}}
            resizeMode="stretch"
            source={props.imageSource }
         />
            {/* Header */}
            <View style={styles.header}>
              <Text h4 style={ {color : colors.mainColor}} >{props.header}</Text>
             </View>
          {/* Content */}

         
                <View style={styles.contentContainer} >
                        <MaterialCommunityIcons name="checkbox-marked-circle-outline" size={24} color={colors.mainColor} />
                        <Text h5 style={styles.content}>{props.description}</Text>
                </View>
            

    
    </Card>
    )
}

const styles = StyleSheet.create({
   header:
   {
      
        alignSelf:'center',
        marginVertical:10
   },
   
   contentContainer:
   {
       flexDirection:'row',
       marginHorizontal:10,
       marginVertical:5,
       
   },
   cardTitle:
   {
       color:colors.mainColor,
       alignSelf:'center',
       margin:5
   },
   content:
   {
    marginLeft:8,
    marginBottom:10,
    marginHorizontal:20,
    fontSize:16
   },
 
})

export default DataCard;