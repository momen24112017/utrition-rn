import {Image, ImageBackground, StyleSheet, Text, View} from 'react-native';
import { PHONE_HEIGHT, PHONE_WIDTH, colors } from './styles';
import React, { Component } from 'react';

import Accordion from 'react-native-collapsible/Accordion';

export default class AccordionShow extends Component {
  state = {
    activeSections: [],
  };

  _renderSectionTitle = section => {
    return (
      <View style={styles.content}>
        <Text>{section.content}</Text>
      </View>
    );
  };

  _renderHeader = section => {
    return (
      <View style={styles.header}>
        <Text style={styles.headerText}>{section.title}</Text>
      </View>
    );
  };

  _renderContent = section => {
    return (
      <View style={styles.content}>
        <Text style={styles.contentText}>{section.content}</Text>
      </View>
    );
  };

  _updateSections = activeSections => {
    this.setState({ activeSections });
  };

  render() {
    return (
    
           
      <Accordion
        sections={this.props.sections}
        activeSections={this.state.activeSections}
        // renderSectionTitle={this._renderSectionTitle}
        renderHeader={this._renderHeader}
        renderContent={this._renderContent}
        onChange={this._updateSections}
      />
     
    );
  }
}
const styles = StyleSheet.create({
    container:
    {
        flex:1,
      
    },
    bannerImage:
    {
        width:PHONE_WIDTH,
        height:PHONE_HEIGHT * 0.2 ,
        resizeMode: 'contain'
    },
    header:
    {
        // width: PHONE_WIDTH,
        // backgroundColor:colors.textGray,
        height:PHONE_HEIGHT * 0.09,
        justifyContent:'center',
        // alignItems:'center',
        borderBottomWidth: 0.5,
        borderColor: colors.mainColor,

       
    },
    headerText :
    {
        fontSize:18,
        color:colors.mainColor,
        fontWeight:'bold',
        marginHorizontal:20
       
    },
    content:
    {
        marginHorizontal:20,
        marginVertical:10
    },
    contentText:
    {
        fontSize:15
    }
})
