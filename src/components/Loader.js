import React from 'react';
import {  StyleSheet, View } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import { colors } from './styles';



  const Loader =() =>  {

    return (
      <View style={styles.container}>
        <Spinner
          visible={true}
          textContent={'Loading...'}
          textStyle={styles.spinnerTextStyle}
          size='large'
          animation='slide'
         
        />

      </View>
    );
  }


const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: colors.textLight
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF'
  },

});

export default  Loader;