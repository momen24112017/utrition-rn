import React from 'react';
import {  StyleSheet , View} from 'react-native';
import { Avatar , Card ,Image ,Text , Button} from 'react-native-elements';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {colors, PHONE_WIDTH} from './styles';



const Mealcard = (props) =>{
    
    return (
        <Card containerStyle={{padding: 0 , minWidth: PHONE_WIDTH * 0.6 , width: PHONE_WIDTH *0.7}}>
 
   
        <View >
          <Image
            style={{ height:200}}
            resizeMode="stretch"
            source={props.imageSource }
         />
            {/* Header */}
            <View style={styles.header}>
              <Text h4 style={ {color : colors.mainColor}} >{props.header}</Text>
             </View>
          {/* Content */}

          {
              props.description.map((item , i=0) => {
                  i++
                return (
                    <View style={styles.contentContainer} key={i}>
                    <MaterialCommunityIcons name="checkbox-marked-circle-outline" size={24} color={colors.mainColor} />
                     <Text h5 style={styles.content}>{item}</Text>
                  </View>
                    )
                    
          })
        }

       
          



        </View>
        <Button
        
        buttonStyle={{ backgroundColor : colors.mainColor, marginVertical:10, marginHorizontal:10}}
        title='VIEW PLAN' 
        onPress={props.onPressButton}
        />
    
</Card>
    )
}

const styles = StyleSheet.create({
   header:
   {
      
        alignSelf:'center',
        marginVertical:10
   },
   contentContainer:
   {
       flexDirection:'row',
       marginHorizontal:10,
       marginVertical:5,
       
   },
   content:
   {
    marginLeft:8
   },
   Button:
   {  
      

    }
})

export default Mealcard;