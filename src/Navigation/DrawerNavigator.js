import React from 'react';
import { createDrawerNavigator, DrawerItem } from '@react-navigation/drawer';
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import AboutScreen from '../Screens/Drawer/AboutScreen';
import FAQScreen from '../Screens/Drawer/FAQScreen';
import HowWorkScreen from '../Screens/Drawer/HowWorkScreen';
import LoginScreen from '../auth/LoginScreen';
import StackNavigator from './StackNavigator';

import {colors} from '../components/styles';

const Drawer = createDrawerNavigator()

const Drawernavigator =() =>{
    
    return(
        <Drawer.Navigator initialRouteName="Home"
            // drawerContentOptions={{
            //     style:{paddingVertical:20, }
                
            // }}
            drawerContentOptions={{
              style:{paddingVertical:20 },
              activeBackgroundColor: colors.mainColor,
              activeTintColor: colors.textLight,
              inactiveTintColor:colors.mainColor
            }}
            
        >
            
                    <Drawer.Screen name="Home" component={StackNavigator} 
                        options={{
                        title: 'Home',
                        
                        drawerIcon: ({focused, size}) => (
                          <FontAwesome
                            name="home"
                            size={size}
                            color={focused ? colors.textLight : colors.mainColor}
                          />
                        ),
                        
                      }}/>
                    <Drawer.Screen name="About" component={AboutScreen} 
                        options={{
                        title: 'About us',
                        drawerIcon: ({focused, size}) => (
                          <FontAwesome
                            name="users"
                            size={size}
                            color={focused ? colors.textLight : colors.mainColor}
                          />
                        ),
                      }}/>
                    <Drawer.Screen name="FAQ" component={FAQScreen} 
                    options={{
                        title: 'FAQ',
                        drawerIcon: ({focused, size}) => (
                            <FontAwesome
                            name="comments-o"
                            size={size}
                            color={focused ? colors.textLight : colors.mainColor}
                            />
                        ),
                        }}/>
                   
                    <Drawer.Screen name="How it Work" component={HowWorkScreen} 
                    options={{
                        title: 'How it Work',
                        drawerIcon: ({focused, size}) => (
                            <FontAwesome
                            name="question-circle"
                            size={size}
                            color={focused ? colors.textLight : colors.mainColor}
                            />
                        ),
                        }}/>
                    <Drawer.Screen name="Login" component={LoginScreen} 
                        options={{
                        title: 'Login',
                        drawerIcon: ({focused, size}) => (
                            <FontAwesome
                            name="sign-in"
                            size={size}
                            color={focused ? colors.textLight : colors.mainColor}
                            />
                        ),
                        }}/>
                        
                </Drawer.Navigator>
    )
}


export default Drawernavigator;