import React from 'react';
import {TouchableOpacity ,StyleSheet} from 'react-native';
import {Icon} from 'react-native-elements';
import {colors} from '../../components/styles';
import { useNavigation} from '@react-navigation/native';

    const HomeIcon =()=>{

      
        const navigation = useNavigation();
        return (
        <TouchableOpacity onPress={() => navigation.navigate('home')}  >
        <Icon 
            name="home"
            size={32}
            color={colors.mainColor}
            style={styles.HomeIconStyle}
            />
        </TouchableOpacity>
        
        )
    }
   
    const styles = StyleSheet.create({
  
        HomeIconStyle:
        {
            alignSelf:'flex-end',
            marginHorizontal:20,
            marginVertical:10
           
            
           
        }
    })
        

export default HomeIcon;
