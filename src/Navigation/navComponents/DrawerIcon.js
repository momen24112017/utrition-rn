import React from 'react';
import {TouchableOpacity ,StyleSheet} from 'react-native';
import {Icon} from 'react-native-elements';
import {colors} from '../../components/styles';
import { useNavigation} from '@react-navigation/native';

    const DrawerIcon =()=>{

      
        const navigation = useNavigation();
        return (
        <TouchableOpacity onPress={() => navigation.openDrawer()}  >
        <Icon 
            name="list"
            size={32}
            color={colors.mainColor}
            style={styles.drawerIconStyle}
            />
        </TouchableOpacity>
        
        )
    }
   
    const styles = StyleSheet.create({
  
        drawerIconStyle:
        {
            alignSelf:'flex-start',
            margin:10,
           
            
           
        }
    })
        

export default DrawerIcon;
// export default withNavigation(DrawerIcon);