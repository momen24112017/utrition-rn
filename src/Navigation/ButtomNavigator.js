import React from 'react';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {Icon} from 'react-native-elements';

//ButtonTab
import HomeScreen from '../Screens/Home/HomeScreen';
import PlanScreen from '../Screens/Home/PlansScreen';
import ContactScreen from '../Screens/Home/ContactScreen';
import NotificationScreen from '../Screens/Home/NotificationScreen';
import ProfileScreen from '../Screens/Home/ProfileScreen';
//Components
import {colors} from '../components/styles';

const buttonTab = createBottomTabNavigator();  //bottonTab object


//Botton Tab Navigator  
const  ButtomNavigator = ()=>{
    return(
      <buttonTab.Navigator tabstyle={{}}
      initialRouteName="home"
      tabBarOptions={{
        activeTintColor:colors.mainColor,
        inactiveTintColor: 'gray',
      }}
      screenOptions={({ route }) => ({
        
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;
          size=22;
          color= focused ?colors.mainColor : color;
  
          switch (route.name) {
            case 'home': iconName = 'home' ; break;
            case 'plans': iconName = 'food' ; break;
            case 'contact us': iconName = 'wechat' ; break;
            case 'notifications': iconName = 'bell' ; break;
            case 'profile': iconName = 'account' ; break;
            
            default: iconName='information-variant'
              break;
          }
          
          
          return <Icon type='material-community' name={iconName} size={size} color={color} />;
        },
      })}
      
      
    >
      <buttonTab.Screen
        name="home"
        component={HomeScreen}
  
       
      />
       <buttonTab.Screen
        name="plans"
        component={PlanScreen}
        
        
      />
     
     <buttonTab.Screen
        name="contact us"
        component={ContactScreen}
        
        
      />
  
     <buttonTab.Screen
        name="notifications"
        component={NotificationScreen}
        
        
      />
  
  <buttonTab.Screen
        name="profile"
        component={ProfileScreen}
        
        
      />
  
  
    
      
    </buttonTab.Navigator>
  
    )
  }

  export default ButtomNavigator;
  