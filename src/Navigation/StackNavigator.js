import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import LoginScreen from '../auth/LoginScreen';
import RegisterScreen from '../auth/RegisterScreen';
import ButtomNavigator from './ButtomNavigator';
import PlanDetails from '../Screens/Additional/PlanDetails';
import Checkout from '../Screens/Additional/Checkout';


const Stack = createStackNavigator()

const StackNavigator = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen
                name="Home"
                component={ButtomNavigator}
                options={{
                    headerShown: false,
                }}
            />
             <Stack.Screen name="login" component={LoginScreen} options={{ headerShown: false }} /> 
            <Stack.Screen name="register" component={RegisterScreen} options={{ headerShown: false }} />
            <Stack.Screen name="Plan Details" component={PlanDetails}  />
            <Stack.Screen name="My Cart" component={Checkout}  />

        </Stack.Navigator>
    )
}

export default StackNavigator;