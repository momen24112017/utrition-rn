export const RegisterURL = 'https://uttrition-test.azurewebsites.net/public/api/register'

export const LoginURL = 'https://uttrition-test.azurewebsites.net/public/api/login'

export const ListPlansURL ='https://uttrition-test.azurewebsites.net/public/api/listPlan'

export const ContactUsURL ='https://uttrition-test.azurewebsites.net/public/api/contactUs'