import React ,{useState} from 'react';
import {View, StyleSheet,TouchableOpacity, ImageBackground, ActivityIndicator} from 'react-native';
import {Text , Button ,Image} from 'react-native-elements'; 
import { ScrollView } from 'react-native-gesture-handler';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {Icon} from 'react-native-elements';

import {PHONE_WIDTH ,PHONE_HEIGHT , colors} from '../../components/styles';




const PlanDetails =({ route, navigation })=>{

  const { item , key} = route.params;
//   const {menuImageWidth , menyImageHight} = useState(null);
  return (
     
  <ScrollView key={item.id}>
    <View style={styles.container} >
        <ImageBackground style={styles.bannerImage} source={{uri: `${item.banner_img}`}}>
            <Text style={styles.price} h4>{item.price_per_day} AED / day</Text>
            <Text style={styles.planName} h3>{item.name}</Text>
        </ImageBackground>

            <View style={styles.contentContainer}>
               
            

            
            <Text style={styles.featureheader}>Why You Need To Follow This Diet ?</Text>
           {  item.long_feature.map(feature => {
            return(
                <View style={styles.featuresContaier} key={feature}>
                    <MaterialCommunityIcons name="checkbox-marked-circle-outline" style={styles.featureIcon}  />
                    <Text style={styles.features}>{feature}</Text>
                </View>
            )
        })
    } 
            
            
            <Text style={styles.shortDesciption} h4>{item.short_desc}</Text>
            <Text style={styles.longDesciption}>{item.long_desc}</Text>
  
            <Button
            title="Buy Now"
            buttonStyle={styles.button}
            icon={
                <Icon
                  name="shopping-cart"
                  size={25}
                  color="white"
                />
            }
            key={item.id}
            onPress={()=> navigation.navigate('My Cart',{item: item , key: item.id})}
            />

            </View>
    </View>

    {/* <ScrollView horizontal>
            <View>
                {Image.getSize(item.menu_sample, (width, height) => {console.log(width, height)})}
                 <Image style={{width:500 , height:400, alignSelf:'flex-start', margin :30}} source={{uri: `${item.menu_sample}`}}/>
            </View>
            </ScrollView> */}

   
    </ScrollView>
  )
}


const styles =StyleSheet.create({
    container:
    {
        backgroundColor: colors.textLight
    },
    
    bannerImage:
    {
        // width: PHONE_WIDTH,
        height:PHONE_HEIGHT * 0.3,
        // justifyContent:'flex-end',
        
    },
    planName :
    {
        color: colors.mainColor,
        backgroundColor:'rgba(255,255,255,0.8)',
        alignSelf:'flex-start',
        paddingHorizontal:5,
        borderRadius:3,
        position:'relative',
        bottom:0,
        top:PHONE_HEIGHT * 0.15
    },
    price:
    {
        alignSelf:'flex-end',
        color: colors.textOrange,
        marginVertical:10,
        backgroundColor:'rgba(255,255,255,0.8)',
        paddingHorizontal:5,
        
    },
    contentContainer:
    {
        marginHorizontal:30
    },
    featureheader:
    {
        color: colors.mainColor,
        fontWeight: 'bold',
        fontSize:18,
        marginVertical:15
        // marginHorizontal:20,
    },
    featuresContaier:
    {
        flexDirection:'row',
        marginHorizontal:10,
        marginVertical:5,
    },
    features:
    {
        fontSize:16,
        alignSelf:'center',
       
    },
    featureIcon:
    {
        color:colors.mainColor,
        fontSize:18,
        padding:5
       
    },
     shortDesciption:
    {
        margin:8,
        color:colors.textOrange,
        alignSelf:'center'

    },
    longDesciption:
    {
        alignSelf:'center',
        // marginHorizontal:20,
        fontSize:17,
        color:colors.textGray,
        textAlign:'center',
        marginBottom:12

    },

    button:
    {
      backgroundColor: colors.mainColor,
      borderRadius:50,
      width :PHONE_WIDTH*0.9,
      paddingVertical:11,
      marginVertical:20

    },

  });


    
export default PlanDetails;

