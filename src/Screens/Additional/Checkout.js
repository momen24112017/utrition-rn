import {ActivityIndicator, Alert, ImageBackground, StyleSheet, TouchableOpacity, View} from 'react-native';
import {Button, Image, Input, Overlay, Text} from 'react-native-elements';
import {PHONE_HEIGHT, PHONE_WIDTH, colors} from '../../components/styles';
import React ,{useState} from 'react';

import DateTimePickerModal from "react-native-modal-datetime-picker";
import Message from '../../components/Message';
import { ScrollView } from 'react-native-gesture-handler';
import moment from 'moment';

const Checkout =({ route, navigation })=>{
    var today = moment()
    var startDate =moment(today, "DD-MM-YYYY").add(2,'d').format('DD-MM-YYYY');
    const { item , key} = route.params;
    const [promocodeState,setPromocodeState] = useState(false);
    const [promocode,setPromocode] = useState(0);
    const [totalAfterPromo , setTotalAfterPromo] =useState(item.price);
    const [date,setDate] = useState(startDate);
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
    const [visible, setVisible] = useState(false);

   
    const showDatePicker = () => {
      setDatePickerVisibility(true);
    };
  
    const hideDatePicker = () => {
      setDatePickerVisibility(false);
    };

    const handleConfirm = (date) => {
        const formattedDate =moment(date).format("DD-MM-YYYY");
        if (formattedDate < startDate){
        // Alert.alert('Please Choose Date after : ' + startDate)`
            setVisible(true);
           
        
        }else{
        setDate(formattedDate)
        }
        hideDatePicker();
    }
    //  console.log(date < startDate);
   const showPromocode =()=>
   {
        !promocodeState ? setPromocodeState(true) : setPromocodeState(false);
        
   }

   const calculateTotalAfterPromo =() =>{
       setPromocode(500)
       setTotalAfterPromo(item.price -promocode)
   }

   const toggleOverlay = () => {
    setVisible(!visible);
  };


  return (
     
  <ScrollView key={item.id}>
        <ImageBackground style={styles.bannerImage} source={{uri: `${item.banner_img}`}}>
            <Text style={styles.price} h4>{item.price_per_day} AED / day</Text>
            <Text style={styles.planName} h3>{item.name}</Text>
        </ImageBackground>
        
        <Message 
         visibility={visible} 
         text={"Please Choose Date after " + startDate}
         onBackdropPress={toggleOverlay}
         />

        <View style={styles.contentContainer}>

        <View style={styles.sectionContainer}>
            <Text style={styles.sectionHeader}>Plan Name : </Text>
            <Text style={styles.sectionValue}>{item.name} </Text>
        </View> 

        <View style={styles.sectionContainer}>
            <Text style={styles.sectionHeader}>Total : </Text>
            <Text style={styles.sectionValue}>{item.price} AED</Text>
        </View>

        <View style={styles.sectionContainer}>
            <Text style={styles.sectionHeader}>Start Date: </Text>
            <Text style={styles.sectionValue}>{date} </Text>
        </View> 

        <View>
        <TouchableOpacity onPress={showDatePicker} >
           <Text style={styles.chooseDateText}>Choose Another Date </Text>
        </TouchableOpacity>
        <DateTimePickerModal
            isVisible={isDatePickerVisible}
            mode="date"
            onConfirm={handleConfirm}
            onCancel={hideDatePicker}
        />

        </View>

        <TouchableOpacity onPress={showPromocode}>
            <Text style={styles.promocodeText}>{promocodeState === false ? 'Enter Promo code' : 'Hide Promo code Area'} </Text>
        </TouchableOpacity>

        {/* Promocode Area */}
        {promocodeState === true?
        <View style={styles.promocodeArea}>
        <Input
            placeholder='Enter Promocode'
            
           
            />
        <Button
            title="Apply"
            buttonStyle={styles.button}
            onPress={calculateTotalAfterPromo}
            />
            </View>
            :null }


        <View style={styles.sectionContainer}>
            <Text style={styles.sectionHeader}>Promocode discout : </Text>
            <Text style={styles.sectionValue}>{promocode} AED</Text>
        </View>

        <View style={styles.sectionContainer}>
            <Text style={styles.sectionHeader}>Total After promocode : </Text>
            <Text style={styles.sectionValue}>{totalAfterPromo} AED</Text>
        </View>

        <Button
            title="Checkout"
            buttonStyle={styles.buttonCheckout}
            
            />
        
        

        </View>
    </ScrollView>
  )
}


const styles =StyleSheet.create({
   
    bannerImage:
    {
        
        height:PHONE_HEIGHT * 0.3,
       
    },
    planName :
    {
        color: colors.mainColor,
        backgroundColor:'rgba(255,255,255,0.8)',
        alignSelf:'flex-start',
        paddingHorizontal:5,
        borderRadius:3,
        position:'relative',
        bottom:0,
        top:PHONE_HEIGHT * 0.15
    },
    price:
    {
        alignSelf:'flex-end',
        color: colors.textOrange,
        marginVertical:10,
        backgroundColor:'rgba(255,255,255,0.8)',
        paddingHorizontal:5,
    },
    contentContainer:
    {
        marginHorizontal:30
    },
    sectionContainer:
    {
        flexDirection: 'row',
        
        marginVertical:15,
        justifyContent: 'space-between'
    },
    sectionHeader:
    {
        alignSelf:'flex-start',
        fontSize:18,
        // fontWeight: 'bold',
        
    }, 
    sectionValue:
    {
        alignSelf:'flex-end',
        color:colors.mainColor,
        fontSize:18,
        fontWeight: 'bold',
        alignItems:'flex-end'
    },
    chooseDateText:{
        fontSize:15,
        color:colors.mainColor,
        alignSelf:'flex-end',
        marginBottom:10
    },
    promocodeText:{
        fontSize:18,
        color:colors.textOrange
    },
    promocodeArea:
    {
        borderWidth:0.5,
        borderRadius:20,
        borderColor:colors.mainColor,
        padding:10,
        marginVertical:10
    },
    button:
    {
      backgroundColor: colors.mainColor,
      borderRadius:50,
      width :'90%',
      paddingVertical:11,
      alignSelf:'center',
      marginBottom:5
      

    },
    buttonCheckout:
    {
        backgroundColor: colors.mainColor,
        borderRadius:50,
        width :PHONE_WIDTH * 0.9,
        paddingVertical:11,
        alignSelf:'center',
        marginHorizontal:30

    },

  });


    
export default Checkout;

