import {Image, StyleSheet, Text, View} from 'react-native';
import { PHONE_HEIGHT, PHONE_WIDTH, colors } from '../../components/styles';
import React, { Component } from 'react';

import Accordion from 'react-native-collapsible/Accordion';
import AccordionShow from '../../components/Accordion';
import DrawerIcon from '../../Navigation/navComponents/DrawerIcon';
import { ScrollView } from 'react-native-gesture-handler';

const SECTIONS = [
  {
    title: 'Why do I need signup ?',
    content: 'We need your details to serve you better & for below For Suggesting the Right Meal Plan that really caters to you based on your health conditions, Intolerances and to help you to achieve your goals.We also need your contact details to organize delivery of your meals at a time & place that suits you',
  },
  {
    title: 'Where do I signup ?',
    content: 'You can sign up through below options :At any of the Golds Gym in Dubai Through our website; Utritionlife.com You can also call +971 526920873 for support from a member of the UTRITION team.​',
  },
  {
    title: 'When can I start ? ',
    content: 'After filling the registration form and on successful payment we can start your plan in as little as two business days. '
  },
  {
    title: 'What is the duration of each plan ?',
    content: 'When you sign up with Utrition, you can choose from the following options of duration and meal delivery frequency: 20 days (5 days a week delivery) To enjoy and save more, you can also sign up for 40 Days or 60 Days.'
  },

 
];

const DELIVERY = [
    {
      title: 'Which areas do you deliver ?',
      content: 'We deliver 6 days a week except Friday throughout Dubai with no cost and based on client requirement we can deliver on Fridays, but it will be charged extra based on location & timing.​',
    },
    {
      title: 'What time do you deliver the food? *',
      content: 'Deliveries are daily. Our morning slot is between 4am and 8am and our evening slot is between 4pm and 10pm. Delivery schedules depend on your specific location but in the event that you’re not available to receive it, we can leave it on your doorstep or deliver to an alternative location.​​',
    },
    {
      title: 'Can it be delivered to my office ? ',
      content: 'Yes, we can during the morning delivery slot or at evening delivery slot based on your choice and availability of delivery slot.'
    },
   
  ];

const CONSULTATION = [
    {
      title: 'When and how often should I have a weigh-in?',
      content: 'We recommend you weigh yourself before starting the meal plan program and once a week at a consistent time (preferably in the morning) to avoid any normal daily weight fluctuations that commonly occur.​​​',
    },
    {
      title: 'Why Should I Consult with a dietitian?',
      content: 'It is recommended to meet with our dietitian, in order to learn about your food preferences, eating habits and lifestyle. We will also provide the In-Body analysis which provides us with a complete review of your current weight, body composition and metabolism. This analysis helps our Utrition dietitian provide you with a fair assessment of what your goals should be and how your diet should be structured in order to achieve your goals.​',
    },
    {
      title: 'What if I don’t want to see a dietitian? ',
      content: 'If you don’t wish to meet with our dietitian, simply select one of our meal plans and continue with the sign-up process. You will be asked for your food intolerances, food allergies, like and dislikes when completing the sign-up form. You can always call us to clarify any concerns you may have.'
    },
   
  ];

  const MEALPLANS = [
    {
      title: 'What meal plans does Utrition offer?',
      content: 'We offer three different meal plans, all our meals are organic, gluten-free, balanced and designed for specific goals including weight loss, weight and nutrition maintenance and muscle gain and recovery. Our Utrition nutritionists are on hand to ensure you select the right plan for you based on your optimal daily calorie intake, age, height, current weight, gender, food intolerances, food allergies, and daily activity level.'
    },
    {
        title: ' How much weight can I expect to lose? ',
        content: 'Losing weight depends on various factors including your age, daily activity, exercise, medical conditions and weight before starting our meal plans. If followed as per the recommend advise form our dietitian, you may lose anything from 4 to 9 kilos per month. Your results all depend on you and your commitment, we are here to help you active that.'
    },
    {
        title: ' Do you offer plans less then one month?   ',
        content: 'In order to change and form healthy habits, Utritrion organic plans are carefully calculated and balanced for a minimum 4-week period. If you wish to try our organic meals for one week, please contact our sales team and they can assist you on a one-week plan. Prices will vary and results are less achievable for any plans less then 4-weeks.'
    },
    {
        title: ' What if I do not want 3 meals per day? ',
        content: 'Utrition menus are carefully designed to ensure you enjoy the optimum quantity of carbs, protein, fat, calories, nutrients and vitamins throughout the day. If you would like to exclude some of the meals, please get in touch and we can discuss options.​        '
      },
      {
          title: ' I’m not interested in losing weight, is there an appropriate plan for me? ',
          content: 'We have different plans to choose from depending on your goals and preferences. Whether for weight loss, weight maintenance or weight gain, we have a plan to suits you!'
      },
      {
          title: ' I want to maintain my weight; do you have a plan for me?​ ',
          content: 'Absolutely! We have two plans to choose from Lean Gluten Free and Lean Paleo. Both of which have a daily calorie intake ranges between 1400 and 1800, which is just enough to maintain a healthy lifestyle.'
      },
      {
        title: ' What if I do heavy exercise/training and want to gain muscle? ​ ',
        content: 'Our "Athlete Paleo" plan is the best plan for you. This plan will help you; stimulate recuperation, absorb nutrients and give you a higher immune function. With a daily calorie intake that ranges from 1800 – 2300, you’ll have enough energy to juice up your training.'
      },
      {
          title: ' How Many meals will I receive?  ',
          content: 'You will receive 3 meals and 1 snack. Breakfast, Lunch, Dinner & 1 snack.'
      },
      {
          title: ' What does Paleo mean? ​  ',
          content: 'Paleo attempts to model what would have been eaten by humans during the Paleolithic era, also known as the prehistoric stone age of human development. This diet includes meats, fish, fruits, vegetables, nuts and seeds. foods that presumably would have been obtained through hunting and gathering. It omits legumes, grains, dairy, refined sugar, low–calorie sweeteners and “processed” foods.'
      },
    
  ];

  const PAYMENTS = [
    {
      title: ' How much will my plan cost?  ',
      content: 'Utrition Plans start from AED 3150, All our plans are tailored based on client intolerances, health condition, allergies and set goals so plan cost will depend on calorie count & Intolerances.​ Currently we are offering 10-15% discount for Golds Gym Members based on membership and for more offers and discount contact Utrition support team @ +971 526920873'
    },
    {
        title: ' Why do I need to pay an AED 200 deposit for the ice and chiller bag? ​ ',
        content: 'We ask for AED 200 as a refundable deposit for a Utrition a chiller bag as experience has shown that our customers love our bags so much, that they’re often reluctant to return them!​'
    },
    {
        title: ' How can I pay? ',
        content: 'We accept cash and cards throughout all the Golds Gym in the UAE. You can pay online with your credit or debit card through New Age Fitness Online Portal or if you prefer, you can drop off payment at our head office in Dubai Outlet Mall. ​'
    },
    {
        title: ' Do you offer discounts?  ',
        content: 'We do have several discounts on offer (Terms and Conditions apply): ​We offer 10-15% discount for Gold Gym Members based on membership type.​Fit Family (10% off); for families to enjoy the plan together​ Refer a friend (one day Detox Plan for each referral upon renewal): as a thank you for spreading the word about Utrition​​Partner in health (10% off for your first month): for companies partnered with Utrition (contact your company HR for more information or if you would like your company to be one of our partners in health). For more customized offers reach our ​ support team @ +971 526920873. '
    },
    
  ]


class FAQScreen extends Component {
  
  render() {
    return (
        <View style={styles.container}> 
             <DrawerIcon/>
        <Image style={styles.bannerImage} source={{uri: 'https://jaxvegancouple.com/wp-content/uploads/2018/08/faq-1024x367.jpg'}}/>
        <ScrollView>
        <View style={styles.section}>
            <Text style={styles.header}>SIGNING-IN</Text>
            <AccordionShow sections={SECTIONS} />
        </View>

        <View style={styles.section}>
            <Text style={styles.header}>DELIVERY</Text>
            <AccordionShow sections={DELIVERY} />
        </View>
        <View style={styles.section}>
            <Text style={styles.header}>CONSULTATION</Text>
            <AccordionShow sections={CONSULTATION} />
        </View>
        

        <View style={styles.section}>
            <Text style={styles.header}>MEAL PLANS</Text>
            <AccordionShow sections={MEALPLANS} />
        </View>

        <View style={styles.section}>
            <Text style={styles.header}>PAYMENTS & DISCOUNTS​</Text>
            <AccordionShow sections={PAYMENTS} />
        </View>
        </ScrollView>
      </View>

     
    );
  }
}
const styles = StyleSheet.create({
    container:
    {
        flex:1,
      
    },
    bannerImage:
    {
        width:PHONE_WIDTH,
        height:PHONE_HEIGHT * 0.2 ,
        resizeMode: 'contain'
    },
    section:
    {
        marginVertical:20,
    },
    header:
    {
        fontSize:24,
        fontWeight:'bold',
        marginHorizontal:20,
        marginBottom:10,
        color:colors.textOrange,
        textAlign:'center'
    }

   
   
})
export default FAQScreen;