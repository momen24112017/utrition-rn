import React from 'react';
import{StyleSheet, View} from 'react-native'
import {Text} from 'react-native-elements';
import {colors} from '../../components/styles';
import DataCard from '../../components/DataCard';
import { ScrollView } from 'react-native-gesture-handler';
import DrawerIcon from '../../Navigation/navComponents/DrawerIcon';

const HowWorkScreen =() => {
    return (
        <View >

            <DrawerIcon/>
            <Text h4 style={styles.title}>HOW IT Work </Text>

        <ScrollView horizontal>
        
        <DataCard 
             header="Know your body" 
             imageSource ={ { uri: 'https://uttrition-test.azurewebsites.net/storage/app/public/plans/August2020/dyW4kvxRw94Zh8vprYpS.jpg', }}
             color ={colors.mainColor}
             description= "Our Life Fitness Specialist will guide you to understand your body type with our Body Composition Analysis. This will enable our Dietitian to insure you will select the right plan and help manage your progress."
        />

        <DataCard 
             header="Select your plan" 
             imageSource ={ { uri: 'https://uttrition-test.azurewebsites.net/storage/app/public/plans/August2020/dyW4kvxRw94Zh8vprYpS.jpg', }}
             color ={colors.mainColor}
             description= "Our Specialists will recommend the meal plan as per your calorie and macronutrients requirements, Food dislikes/allergies and your special instructions"
        />

       <DataCard 
             header="Enjoy your results" 
             imageSource ={ { uri: 'https://uttrition-test.azurewebsites.net/storage/app/public/plans/August2020/dyW4kvxRw94Zh8vprYpS.jpg', }}
             color ={colors.mainColor}
             description= "We only use the freshest, highest quality ingredients cooked by our top executive chef. You will receive constant support throughout the entire period of the plan from your designated Utrition account manager"
      />

    </ScrollView>
    </View>
    )
}
const styles = StyleSheet.create({
    title:
    {
        color: colors.mainColor,
        alignSelf:'center',
        marginVertical:20
    },

})

export default HowWorkScreen;