import { Card, Image, Text } from 'react-native-elements';
import {StyleSheet, View} from 'react-native';

import DataCard from '../../components/DataCard';
import DrawerIcon from '../../Navigation/navComponents/DrawerIcon';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import React from 'react';
import { ScrollView } from 'react-native-gesture-handler';
import {colors} from '../../components/styles';

const AboutScreen =() => {
    return (
        <View style={styles.container}>
            <DrawerIcon/>
         
           <ScrollView>
            <View  style={styles.logoContainer}>
                
                <Image
                    source={{uri : 'https://uttrition-utrition.azurewebsites.net/assets/images/new-ethos.png'}}
                    style={styles.logo}
                    resizeMode="contain"
                />
                <Text  style={styles.ethosText}>Here at Utrition we believe in the goals of our clients. We are here to accompany our clients on their journey to success in both training and nutrition.</Text>
                <Text  style={styles.ethosText}>Our meal plans are designed to integrate with your training program seamlessly, be it our accuracy on your required macronutrients, reducing inflammation or support your muscle recuperation.</Text>
                <Text  style={styles.ethosText}>Based on your body analysis, our nutritionist & master chef work together to design a complete meal plan that you can indulge in and meet your wellness goals. Setting goals is a start but achieving goals is the real win.</Text>
                </View>
            
            <View style={styles.titleContainer} >
                <Text style={{color: colors.mainColor}} h4>FRESH</Text>
                <Text style={{color: colors.textOrange}} h4>-ORGANIC-</Text>
                <Text style={{color: colors.mainColor}} h4>DELIVERED</Text>

                
            </View>
            
            <ScrollView horizontal>
        
           

            {/* Fresh card  */}

            <DataCard 
                header="Fresh"
                imageSource={{ uri:  "https://wwwextendaretail.cdn.triggerfish.cloud/uploads/2019/04/fresh_food_transparency_header.jpg", }}
                color={colors.mainColor}
                description="sourced ingredients cooked by our top executive chef"
               />

            <DataCard 
                header="Organic"
                imageSource={{ uri:  "https://www.cairowestmag.com/wp-content/uploads/2018/04/fruit_vegetables_food_produce_organic.png", }}
                color={colors.mainColor}
                description="sourcing, Macro-Balanced, Low-Carb and vegan options"
               />


             <DataCard 
                header="Delivered"
                imageSource={{ uri:  "https://www.racv.com.au/content/dam/racv/images/royalauto-2019/living/home/food-delivery-safety/food-box-thumb2.jpg", }}
                color={colors.mainColor}
                description="to your doorstep daily with your health and safety at heart"
               />



        
        </ScrollView>
        </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    container:
    {
        backgroundColor:colors.textLight ,
         paddingBottom:100
    },
    titleContainer:
    {
        flexDirection:'row',
        alignSelf:'center',
        marginVertical:20
    },
    logoContainer:{
        alignSelf:'center',
        alignItems:'center'
        // marginVertical:50, 
    },
    logo :
    {
         width: 150,
         height: 150 ,  
         
    },
    ethosText:
    {
        marginHorizontal:20,
        textAlign:'justify',
        fontSize:16
    }
    
})

export default AboutScreen;