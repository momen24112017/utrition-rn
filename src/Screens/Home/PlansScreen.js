import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';

import {ButtonGroup} from 'react-native-elements';
import DrawerIcon from '../../Navigation/navComponents/DrawerIcon';
import React from 'react';
import { colors } from '../../components/styles';

class PlanScreen extends React.Component {


  constructor () {
    super()
    this.state = {
      selectedIndex: 0
    }
    this.updateIndex = this.updateIndex.bind(this)
  }
  updateIndex (selectedIndex) {
    this.setState({selectedIndex})
  }
  
  
  
  render () {
    const component1 = () => <Text>Menu</Text>
  const component2 = () => <Text>Renew</Text>
  const component3 = () => <Text>Submit plan</Text>
    const buttons = [{ element: component1 }, { element: component2 }, { element: component3 }]
    const { selectedIndex } = this.state
    return (
      <View style={styles.container}>
        <DrawerIcon/>
      <ButtonGroup
        onPress={this.updateIndex}
        selectedIndex={selectedIndex}
        buttons={buttons}
        containerStyle={{height: 50 }}
        selectedButtonStyle={styles.buttons}
        />
        <Text style={{alignSelf:'center', marginTop:50}}>No thing to show</Text>
        </View>
    )
  }

}


  const styles =StyleSheet.create({
    container:
    {
      flex:1,
      backgroundColor: colors.textLight
    },
    buttons:
    {
      backgroundColor: colors.mainColor,
      color: colors.textLight
    }
  })

export default PlanScreen;
