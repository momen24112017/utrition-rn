import React from 'react';
import { StyleSheet,  View, ScrollView, Dimensions} from 'react-native';
import {Text} from 'react-native-elements';
import { FlatList } from 'react-native-gesture-handler';

import {colors , PHONE_WIDTH } from '../../components/styles'
import DrawerIcon from '../../Navigation/navComponents/DrawerIcon';

export default class NotificationScreen extends React.Component {
    
  render() {

    const RECENTDATA = [
      {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
        title: 'Your meal is at the  way to you',
        
      },
      {
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
        title: 'Your meal is at the  way to you',
       
      },
      {
        id: '58694a0f-3da1-471f-bd96-145571e29d72',
        title:'Your meal is at the  way to you',
        
      },
      {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53a',
        title:'Your meal is at the  way to you'
      },

    ]
    const DATA = [
      {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
        title: 'Your meal is at the  way to you'
      },
      {
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
        title: 'Your meal is at the  way to you'
      },
      {
        id: '58694a0f-3da1-471f-bd96-145571e29d72',
        title:'Your meal is at the  way to you'
      },
      {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53a',
        title:'Your meal is at the  way to you'
      },
      {
        id: '3ac68afc-c605-48d3-a4f8-fb7f63',
        title: 'Your meal is at the  way to you'
      },
      {
        id: '58694a0f-3da1-471f-bd96-149d72',
        title: 'Your meal is at the  way to you'
      },
      {
        id: 'bd7acbea-46c2-aed5-3ad53abb28ba',
        title: 'Your meal is at the  way to you',
         
      },
      {
        id: '3ac68afc-48d3-a4f8-fbd91aa97f63',
        title: 'Your meal is at the  way to you',
        
      },
      {
        id: '58694a0f-471f-bd96-145571e29d72',
        title: 'Your meal is at the  way to you',
        
      },
      {
        id: 'bdcbea-c1b1-46c2-aed5-3ad53a',
        title: 'Your meal is at the  way to you',
         
      },
      {
        id: '3ac6fc-c605-48d3-a4f8-fb7f63',
        title: 'Your meal is at the  way to you',
        
      },
      {
        id: '586a0f-3da1-471f-bd96-149d72',
        title: 'Your meal is at the  way to you',
        price: '200 $'
      },
    ];

    return (
      
      <View style={styles.container}>
        <DrawerIcon/>
        <Text style={{marginLeft:20, marginBottom:1 , color: colors.mainColor}} h3>Notification</Text>
        <ScrollView>
        <View style={styles.cardsContainer }>
        <Text style={{marginLeft:20, marginBottom:5, color: colors.mainColor}} h5>Recent</Text>
          <FlatList
             data={RECENTDATA}
             renderItem={({item}) => 
            
            <View style={styles.cardContainer}>
              <View style={styles.letterContainer}><Text style={styles.letter}>N</Text></View>
             <Text style={styles.cardTitle}>{item.title}</Text>
             <View style={styles.dateContainer}>
             <Text style={{marginHorizontal:5, alignContent:'center'}}>today</Text>
             </View>
             </View>

            }
             keyExtractor={item => item.id}
             />

          
        <Text style={{marginLeft:20, marginBottom:15 , color: colors.mainColor}} h5>Earlier</Text>
          <FlatList
             data={DATA}
             renderItem={({item}) => 
            
            <View style={styles.cardContainer}>
              <View style={styles.letterContainer}><Text style={styles.letter}>N</Text></View>
             <Text style={styles.cardTitle}>{item.title}</Text>
             <View style={styles.dateContainer}>
             <Text style={{marginHorizontal:5, alignContent:'center'}}>2 day</Text>
             </View>
             </View>

            }
             keyExtractor={item => item.id}
             />

        </View>
</ScrollView>

      



      </View>
      
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    

  },
 
  cardsContainer:{

    width: PHONE_WIDTH    
  },
  cardContainer:{
    flexDirection:'row',
    backgroundColor:colors.textLight,
    borderWidth:0.1,
    borderColor: colors.textDark,
    marginVertical:3,
    width: PHONE_WIDTH,
   
    
  },
  letterContainer:{
    height:50,
    width:50,
    justifyContent:'center',
    backgroundColor: colors.mainColor,
    marginHorizontal:10,
    marginVertical:10,
    borderRadius:30
  },
  letter:{
    color: '#fff',
    fontWeight:'bold',
    fontSize:20,
    alignSelf:'center'

  },

  cardTitle:{
    fontSize:15,
    marginHorizontal:10,
    marginBottom:15,
    marginTop:10,
    width: PHONE_WIDTH,
    flex: 1, 
    

  },
  dateContainer:{
    height:50,
    width:50,
    justifyContent:'center',
    
    marginHorizontal:10,
    marginVertical:10,
    borderRadius:30
  },
});
