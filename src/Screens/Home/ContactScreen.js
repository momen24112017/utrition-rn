import React from 'react';
import {StyleSheet, View , TouchableOpacity} from 'react-native';
import {Text , Input ,  Button} from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome';

import {colors , PHONE_WIDTH} from '../../components/styles';
import DrawerIcon from '../../Navigation/navComponents/DrawerIcon';
import ErrorText from '../../components/ErrorText';
import {validateEmail} from '../../components/Validation';
import Loader from '../../components/Loader'

//Redux
import {connect } from 'react-redux';
import {contactUs} from '../../actions';


class ContactScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        firstName:'',
        lastName:'',
        email :'',
        phone:'',
        message:'',
        errorMessage:null,
        loading:false
        
    };
    this.handleSubmit = this.handleSubmit.bind(this);
}
  isEmpty(){
    const {firstName, lastName , email, phone, message} =this.state;
    if(firstName === ''  || lastName === '' || email === '' || phone === '' || message ===''){
     
     return true
    }
    else{
      return false
    }
  }

  deleteAllData(){
    this.setState({
      firstName: '',
      lastName: '',
      email:'',
      phone:'',
      message:'',
      errorMessage:null

    })
  }

  showLoader =()=>{
    this.setState({loading:true})
    }

  hideLoader =()=>{
    this.setState({loading: false}) 
  }


  async handleSubmit(){ 
      const {firstName, lastName , email, phone, message } =this.state;
      if(this.isEmpty())
      {
        this.setState({errorMessage: 'Please Fill Fields'})
      }
      else if (!validateEmail(email))
      {
        this.setState({errorMessage: 'Please Enter Valid E-mail'})
      }
      else
      {
        this.showLoader()
      const result =  await this.props.contactUs(firstName,lastName,email,phone,message);
          
        if (result) {
          this.deleteAllData();
          this.hideLoader();
        }
      }
      
}
    render(){
      const {firstName, lastName , email, phone, message , errorMessage ,loading} =this.state;
        return(

          <View style={{backgroundColor:colors.textLight}}>
            {/* Loader */}
            {loading ? <Loader/> : null} 
             <DrawerIcon/>
          <ScrollView style={styles.container}>
           
            <View  style={styles.dataContainer}>
            {/* Title */}
              <Text style={styles.title} h4>Working hours</Text>
              <Text>9AM - 8PM, SATURDAY TO THURSDAY</Text>
              <Text style={styles.contactData}>Tel:(+971) 526920873</Text>
              <Text  style={styles.contactData}>E-mail:info@utritionlife.com</Text>

              <View style={styles.formContainer}>
              
              <Input
                label="First Name"
                placeholder='First Name'
                leftIcon={ <Icon name='user' size={24}color={colors.mainColor} style={styles.inputIcon}/>}
                labelStyle={styles.labelStyle}
                containerStyle={styles.inputContainer}
                inputContainerStyle={styles.input}
                value={firstName}
                onChangeText={(firstName) => this.setState({firstName})}
                requ

                //Next Button
                ref={ref => {this.firstname = ref;}} 
                returnKeyType = { "next" }
                onSubmitEditing={() => { this.lastname.focus(); }}
                
            />

            <Input
                label="Last Name"
                placeholder='Last Name'
                leftIcon={ <Icon name='user' size={24}color={colors.mainColor} style={styles.inputIcon}/>}
                labelStyle={styles.labelStyle}
                containerStyle={styles.inputContainer}
                inputContainerStyle={styles.input}
                value={lastName}
                onChangeText={(lastName) => this.setState({lastName})}


                //Next Button
                ref={ref => {this.lastname = ref;}} 
                returnKeyType = { "next" }
                onSubmitEditing={() => { this.email.focus(); }}
                
            />

            <Input
                label="Email"
                placeholder='Email'
                leftIcon={ <Icon name='envelope-o' size={24}color={colors.mainColor} style={styles.inputIcon}/>}
                labelStyle={styles.labelStyle}
                containerStyle={styles.inputContainer}
                inputContainerStyle={styles.input}
                textContentType='emailAddress'
                 value={email}
                onChangeText={(email) => this.setState({email})}


                //Next Button
                ref={ref => {this.email = ref;}} 
                returnKeyType = { "next" }
                onSubmitEditing={() => { this.phone.focus(); }}
                
            />  

           <Input
                label="Phone"
                placeholder='Phone'
                leftIcon={ <Icon name='phone' size={24}color={colors.mainColor} style={styles.inputIcon}/>}
                labelStyle={styles.labelStyle}
                containerStyle={styles.inputContainer}
                inputContainerStyle={styles.input}
                keyboardType='numeric'
                value={phone}
                onChangeText={(phone) => this.setState({phone})}


                //Next Button
                ref={ref => {this.phone = ref;}} 
                returnKeyType = { "next" }
                onSubmitEditing={() => { this.message.focus(); }}
                
            />

              <Input
                // label="Message"
                placeholder="Message"
                labelStyle={styles.labelStyle}
                containerStyle={styles.textAreaContainer}
                inputContainerStyle={styles.textArea}
                textContentType='none'
                multiline
                value={message}
                onChangeText={(message) => this.setState({message})
              }


                ref={ref => {this.message = ref;}} 
              
                
            />  
          {errorMessage ?  <ErrorText message={errorMessage}/>: null}
          <Button
            title="Send"
            containerStyle={styles.buttonContainer}
            buttonStyle={styles.button}
            onPress={this.handleSubmit}
            />

        </View>
          </View>
          </ScrollView>
          </View>
        )
    }
}

const styles = StyleSheet.create({
  container:
  {
    backgroundColor: colors.textLight,
    
  },
  dataContainer: 
  {
    alignItems:'center',
    alignContent:'center',
    justifyContent:'center'
  },
  title:
  {
    // alignSelf:'center',
    color: colors.mainColor,
    margin:10
  },
  contactData:
  {
    color:colors.textGray
  },
  formContainer :
  {
    marginVertical:40
  },
  labelStyle:
  {
      marginLeft:10,
      
  },
  inputContainer :
  {
      width :PHONE_WIDTH*0.9,
      marginVertical:-7
      
  },
  inputIcon:
  {
      marginRight:5
  },
  input:
  {
     borderBottomWidth:0.5,
      borderWidth : 0.5,
      borderColor: colors.mainColor,
      borderRadius: 50,
      paddingHorizontal:15,
      marginTop:5,
      
      
  },
  textAreaContainer:
  {
    width :PHONE_WIDTH*0.9,
    minHeight:150,
    borderBottomWidth:0.5,
    borderWidth : 0.5,
    borderColor: colors.mainColor,
    borderRadius: 20,
    marginTop:5,
    marginBottom:20,
    
    
    
  },
  textArea:
  {
   borderBottomWidth:0
  },
  
  button:
  {
    backgroundColor: colors.mainColor,
    borderRadius:50,
    width :PHONE_WIDTH*0.9,
    paddingVertical:11,
    marginBottom:30

  },
})

const mapStateToProps =state => {
  return {contactReducer : state.contactUs}
}

export default connect(mapStateToProps, {contactUs}) (ContactScreen);
