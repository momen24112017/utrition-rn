import React from 'react';
import {StyleSheet, View} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import {Text , Card} from 'react-native-elements'

//Components
import {colors , PHONE_HEIGHT} from '../../components/styles';
import DrawerIcon from '../../Navigation/navComponents/DrawerIcon';

class ProfileScreen extends React.Component {
    render(){
        return(
            <View style={styles.container}>
                <DrawerIcon/>
                <ScrollView>
                     <Text style={styles.welcomText} h3> Hesham Gamal</Text>


                    {/* Personal information card */}
                     <Card>
                        <Card.Title h4 style={styles.header}>Personal Information</Card.Title>
                        <Card.Divider/>
                        {
                           
                                <View >

                                <View style={styles.sectionContainer }>
                                    <Text style={styles.sectionName}>Name : </Text>
                                    <Text style={styles.sectionData}>Hesham Gamal</Text>
                                </View>

                                <View style={styles.sectionContainer }>
                                    <Text style={styles.sectionName}>Email : </Text>
                                    <Text style={styles.sectionData}>hesham@girafee-code.com</Text>
                                </View>

                                <View style={styles.sectionContainer }>
                                    <Text style={styles.sectionName}>Phone : </Text>
                                    <Text style={styles.sectionData}>01123456789</Text>
                                </View>

                                </View>
                           
                        }
                        </Card>


                        {/* Delivery information Data */}

                        <Card>
                        <Card.Title h4 style={styles.header}>Delivery Information</Card.Title>
                        <Card.Divider/>
                        {
                           
                                <View >
                                <Text h5>Addresss</Text>
                                <View style={styles.sectionContainer }>
                                    <Text style={styles.sectionName}>Home : </Text>
                                    <Text style={styles.sectionData}>Nasr city</Text>
                                </View>

                                <View style={styles.sectionContainer }>
                                    <Text style={styles.sectionName}>Work: </Text>
                                    <Text style={styles.sectionData}>6th of october</Text>
                                </View>

                                

                                </View>
                           
                        }
                        </Card>

                        {/* Medical Information Card */}

                        <Card>
                        <Card.Title h4 style={styles.header}>Medical Information</Card.Title>
                        <Card.Divider/>
                        {
                           
                                <View >
                                <Text h5>Medical Condition</Text>
                                <View >
                                    
                                    <Text style={styles.sectionData}>Diabets</Text>
                                    <Text style={styles.sectionData}>Blood Pressure</Text>
                                </View>

                               

                                

                                </View>
                           
                        }
                        </Card>
                        
                </ScrollView>
            </View>
        )
    }
}

const styles= StyleSheet.create({
    container :
    {
        flex:1,
        // backgroundColor : colors.textLight,
    }
    ,
    welcomText:
    {
        color: colors.mainColor,
        marginTop:50,
        marginBottom:20,
        marginHorizontal:10,
        alignSelf:'center'
    },
    header:
    {
        color: colors.mainColor
    },
    sectionContainer :
    {
        flexDirection:'row',
        marginVertical:8

    },
    sectionData:
    {
        marginLeft:20
    }


})
export default ProfileScreen;