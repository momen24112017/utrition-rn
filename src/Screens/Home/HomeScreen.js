import {ActivityIndicator, StyleSheet, TouchableOpacity, View} from 'react-native';
import {Icon, Text} from 'react-native-elements';
import {PHONE_HEIGHT, PHONE_WIDTH, colors} from '../../components/styles';

import DrawerIcon from '../../Navigation/navComponents/DrawerIcon';
import MealCard from '../../components/MealCard'
import React from 'react';
import { ScrollView } from 'react-native-gesture-handler';
import Story from '../../components/Story';
import {connect} from 'react-redux';
import {listPlans} from '../../actions/index'

var STORY =[
    
      
       {
             uri: 'https://headtosuccess.com/wp-content/uploads/2017/11/What-Makes-a-Person-Successful-Reviews.jpg',
             header: 'Head To Success',
             content: 'It is great time to help others' ,
        },
        {
            uri: 'https://www.incimages.com/uploaded_files/image/1920x1080/getty_497937880_274146.jpg',
            header: 'Habits of the Most Successful People',
            content: 'Smile help you '
        },
        {
            uri: 'https://www.lifegoalmotivations.com/wp-content/uploads/2020/05/af.jpg',
            header: ' Challenge yourself',
            content: 'Richard Branson says his biggest motivation is to keep challenging himself. He treats life like one long university education, where he can learn more every day. You can too!',
        },
        {
            uri: 'https://www.potential.com/wp-content/uploads/2017/03/career-succes.png',
            header: 'Do work you care about',
            content: 'There’s no doubt that running a business take a lot of time. Steve Jobs noted that the only way to be satisfied in your life is to do work that you truly believe in.',
        },
        {
            uri: 'https://images.unsplash.com/photo-1533227268428-f9ed0900fb3b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80',
            header: 'Take the risk',
            content: 'We never know the outcome of our efforts unless we actually do it. Jeff Bezos said it helped to know that he wouldn’t regret failure, but he would regret not trying.',
        },
        {
            uri: 'https://cdn.lifehack.org/wp-content/uploads/2015/07/successful.jpg',
            header: 'Believe in yourself',
            content: 'As Henry Ford famously said, “Whether you think you can, or think you can’t, you’re right.” Believe that you can succeed, and you’ll find ways through different obstacles. If you don’t, you’ll just find excuses.',
        },
        {
            uri: 'https://www.incimages.com/uploaded_files/image/1920x1080/getty_500303866_130909.jpg',
            header: 'Have a vision',
            content: 'The founder and CEO of Tumblr, David Karp, notes that an entrepreneur is someone who has a vision for something and a desire to create it. Keep your vision clear at all times.',
        },
        {
            uri: 'https://images.pexels.com/photos/6945/sunset-summer-golden-hour-paul-filitchkin.jpg?auto=compress&cs=tinysrgb&dpr=1&w=500',
            header: 'Find good people',
            content: 'Who you’re with is who you become. Reid Hoffman, co-founder of LinkedIn, noted that the fastest way to change yourself is to hang out with people who are already the way you want to be.',
        },
        {
            uri: 'https://images.unsplash.com/photo-1519834785169-98be25ec3f84?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80',
            header: 'Face your fears',
            content: 'Overcoming fear isn’t easy, but it must be done. Arianna Huffington once said that she found fearlessness was like a muscle -- the more she exercised it, the stronger it became.',
        },
    
        ]


class HomeScreen extends React.Component {
   async getPlans(){

    await   this.props.listPlans(); 
   }

    componentDidMount(){
      this.getPlans();
    }

    planList(){
     if(!this.props.plans.data){
         return( 
         <View style={{margin:PHONE_WIDTH * 0.4}}>
         <ActivityIndicator size="large" color={colors.mainColor} />
         <Text>Loading</Text>
         </View>
         )
     }else{
        return this.props.plans.data.map(plan => {
            return(
                <TouchableOpacity 
                key={plan.id}
                    onPress={()=> this.props.navigation.navigate('Plan Details',{item: plan , key: plan.id})}
                >
                <MealCard 
                   
                    header={plan.name} 
                    imageSource ={ { uri: `${plan.feature_img}` }}
                    color ={plan.color}
                    description= {plan.feature}
                    onPressButton={()=> this.props.navigation.navigate('Plan Details',{item: plan , key: plan.id})}
                    />
                    </TouchableOpacity>
            )
        })
    }
    }

    renderStories(){

        return STORY.map(story => {
            return(
            <Story
                key={story.uri}
                image ={{uri:  story.uri}}
                header={story.header}
                content={story.content}
            />
            )
    })
}

    render(){
     
        return  (
            <ScrollView >
            <View  style={styles.container}>
            
               <DrawerIcon/>
              
              
                <View style={styles.storyContainer} >
            <Text style={styles.header}>Success Stories</Text>
                {/* Story */}
                <ScrollView  horizontal>
                  
                    {this.renderStories()}
              
                  
                </ScrollView>
                </View>
                
                {/* Meal Plans */}
                <View>
                    <Text h4 style={styles.plansHeader}>Meal Plans</Text>
                    <ScrollView  horizontal>
                    {this.planList()}

                    </ScrollView>
                </View>



          </View>
          </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container :
    {
        flex: 1,
        height: PHONE_HEIGHT,
        backgroundColor : colors.textLight,
    },
    drawerIconStyle:
    {
        alignSelf:'flex-start',
        margin:10,
       
    },
    storyContainer:
    {
        marginVertical:10,
        marginHorizontal:10,
        
        
    },
    header :
    {
        color:colors.mainColor,
        fontSize:15,
        margin:5
    },
    plansHeader:
    {
        color:colors.mainColor,
        marginLeft:20
    }


    
})
const mapStateToProps =state => {
    return {plans : state.plans}
}


export default connect(mapStateToProps, {listPlans}) (HomeScreen);
// export default HomeScreen;