import React from 'react';
import {Text ,StyleSheet, View, SafeAreaView} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import { Input , Image , Button}  from 'react-native-elements';
import {colors  ,PHONE_WIDTH} from '../components/styles';
import { ScrollView } from 'react-native-gesture-handler';
import HomeIcon from '../Navigation/navComponents/HomeIcon';
import ErrorText from '../components/ErrorText';
import {validateEmail} from '../components/Validation';
import Loader from '../components/Loader'
//Redux
import {connect } from 'react-redux';
import {register} from '../actions';




class RegisterSceen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name:'',
            email :'',
            address:'',
            age:'',
            address:'',
            errorMessage: null,
            loading : false
            
        };
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    isEmpty(){
        const {name , email , password , age , phone, address} =this.state;
        if(name === '' || email === '' , password === '' || age === ''|| phone === '' || address ===''){
         
         return true
        }
        else{
          return false
        }
      }

      showLoader =()=>{
        this.setState({loading:true})
        }

      hideLoader =()=>{
        this.setState({loading: false}) 
      }


   async handleSubmit(){
        const {name , email , password , age , phone, address} =this.state;
        
        if(this.isEmpty())
        {
          this.setState({errorMessage: 'Please Fill Fields'})
        }
        else if (!validateEmail(email)){
          this.setState({errorMessage: 'Please Enter Valid E-mail'})
        }
        else{
            this.showLoader()
            const result= await this.props.register(name,email ,password,age,phone,address);
            if(result)
            {
             this.props.navigation.navigate('home')
             this.hideLoader()
             this.setState({errorMessage: ''});
            }else
            {
                this.hideLoader()
            }
        }
    }




    render(){
        const {name , email , password , age , phone, address, errorMessage, loading} =this.state;
        return  (
            <View style={{backgroundColor:colors.textLight}}>
                {/* Loader */}
                {loading ? <Loader/> : null} 
                <HomeIcon/>
           
            <ScrollView>
            <View  style={styles.container}>
            <View  style={styles.logoContainer}>
            <Image
                source={{uri : 'https://uttrition-uttrition-website.azurewebsites.net/images/logo_crop.jpg'}}
                style={styles.logo}
                resizeMode="contain"
            />
            </View>
            
            <Input
                label="Name"
                placeholder='Name'
                leftIcon={ <Icon name='user' size={24}color={colors.mainColor} style={styles.inputIcon}/>}
                labelStyle={styles.labelStyle}
                containerStyle={styles.inputContainer}
                inputContainerStyle={styles.input}
                value={name}
                onChangeText={(name) => this.setState({name})}

                //Next Button
                ref={ref => {this.name = ref;}} 
                returnKeyType = { "next" }
                onSubmitEditing={() => { this.email.focus(); }}
                
            />

            <Input
                label="Email"
                placeholder='Email'
                leftIcon={ <Icon name='envelope-o' size={24}color={colors.mainColor} style={styles.inputIcon}/>}
                labelStyle={styles.labelStyle}
                containerStyle={styles.inputContainer}
                inputContainerStyle={styles.input}
                textContentType='emailAddress'
                value={email}
                onChangeText={(email) => this.setState({email})}

                //Next Button
                ref={ref => {this.email = ref;}} 
                returnKeyType = { "next" }
                onSubmitEditing={() => { this.password.focus(); }}
                
            />

            <Input
                label="Password"
                placeholder='Password'
                leftIcon={ <Icon name='lock' size={24}color={colors.mainColor} style={styles.inputIcon}/>}
                labelStyle={styles.labelStyle}
                containerStyle={styles.inputContainer}
                inputContainerStyle={styles.input}
                textContentType='password'
                secureTextEntry={true}
                value={password}
                onChangeText={(password) => this.setState({password})}

                //Next Button
                ref={ref => {this.password = ref;}} 
                returnKeyType = { "next" }
                onSubmitEditing={() => { this.age.focus(); }}
                
            />

            <Input
                label="Age"
                placeholder='Age'
                leftIcon={ <Icon name='calendar' size={24}color={colors.mainColor} style={styles.inputIcon}/>}
                labelStyle={styles.labelStyle}
                containerStyle={styles.inputContainer}
                inputContainerStyle={styles.input}
                keyboardType='numeric'
                value={age}
                onChangeText={(age) => this.setState({age})}

                //Next Button
                ref={ref => {this.age = ref;}} 
                returnKeyType = { "next" }
                onSubmitEditing={() => { this.phone.focus(); }}
                
            />

            <Input
                label="Phone"
                placeholder='Phone'
                leftIcon={ <Icon name='phone' size={24}color={colors.mainColor} style={styles.inputIcon}/>}
                labelStyle={styles.labelStyle}
                containerStyle={styles.inputContainer}
                inputContainerStyle={styles.input}
                keyboardType='numeric'
                value={phone}
                onChangeText={(phone) => this.setState({phone})}

                //Next Button
                ref={ref => {this.phone = ref;}} 
                returnKeyType = { "next" }
                onSubmitEditing={() => { this.address.focus(); }}
                
            />

            <Input
                label="Address"
                placeholder='Address'
                leftIcon={ <Icon name='home' size={24}color={colors.mainColor} style={styles.inputIcon}/>}
                labelStyle={styles.labelStyle}
                containerStyle={styles.inputContainer}
                inputContainerStyle={styles.input}
                value={address}
                onChangeText={(address) => this.setState({address})}


                 //Next Button
                 ref={ref => {this.address = ref;}} 
                
            />

            {errorMessage ?  <ErrorText message={errorMessage}/>: null}


            <Button
            title="Register"
            containerStyle={styles.buttonContainer}
            buttonStyle={styles.button}
            onPress={this.handleSubmit}
            />


        <View style={styles.loginContainer}>
            <Text style={styles.createTextNormal}>Already have an account ? </Text>
            <Text style={styles.createTextColored} onPress={()=> this.props.navigation.navigate('login')}>Login</Text>
        </View>


          </View>
          </ScrollView>
          </View>
        )
    }
}

const styles = StyleSheet.create({
    container :
    {
        flex: 1,
        alignItems: 'center',  
        backgroundColor : colors.textLight,
    },
    logoContainer:{
        alignSelf:'center',
        marginVertical:50, 
    },
    logo :
    {
         width: 100,
         height: 100 ,  
         
    },
    labelStyle:
    {
        marginLeft:10,
        
    },
    inputContainer :
    {
        width :PHONE_WIDTH*0.9,
        marginVertical:-7
        
    },
    inputIcon:
    {
        marginRight:5
    },
    input:
    {
       borderBottomWidth:0.5,
        borderWidth : 0.5,
        borderColor: colors.mainColor,
        borderRadius: 50,
        paddingHorizontal:15,
        marginTop:5,
        
    },
    buttonContainer:{
        
       
    },
    button:
    {
      backgroundColor: colors.mainColor,
      borderRadius:50,
      width :PHONE_WIDTH*0.9,
      paddingVertical:11

    },
    loginContainer:{
        flexDirection:'row',
        alignContent:'center',
        marginHorizontal: 40,
        marginTop:10,
        marginBottom:100
        
    },
    createTextNormal:{
        fontSize:15,
    },
    createTextColored:{
        fontSize:15,
        color:colors.mainColor,
    },
})
const mapStateToProps =state => {
    return {registerReducer : state.register}
}

export default connect(mapStateToProps, {register}) (RegisterSceen);
