import React from 'react';
import {Text ,StyleSheet, View} from 'react-native';


import Icon from 'react-native-vector-icons/FontAwesome';
import { Input , Image , Button }  from 'react-native-elements';
import {colors  ,PHONE_WIDTH , PHONE_HEIGHT} from '../components/styles';
import { ScrollView } from 'react-native-gesture-handler';
import {connect } from 'react-redux';
import * as Progress from 'react-native-progress';

import HomeIcon from '../Navigation/navComponents/HomeIcon';
import {login} from '../actions';
import ErrorText from '../components/ErrorText';
import Loader from '../components/Loader'



class LoginScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email :'',
            password:'',
            errorMessage:'',
            loading : false
            
        };
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    
    isEmpty(){
        const {email , password } =this.state;
        if( email === '' , password === ''){
         
         return true
        }
        else{
          return false
        }
      }

      showLoader =()=>{
        this.setState({loading:true})
        
      }
      hideLoader =()=>{
        this.setState({loading: false}) 
      }



   async handleSubmit(){
         
        if (this.isEmpty()) {
            this.setState({errorMessage: 'Please Fill Fields'})
           
        }else{


            this.showLoader()
            const result = await this.props.login(this.state.email , this.state.password);
             if(result){ 
             this.props.navigation.navigate('home')
             this.hideLoader()
             this.setState({errorMessage: '' , email: '' , password: ''});
             }else{
             this.hideLoader()
             }
    
        }   
    }

    render(){
        const {email , password , errorMessage, loading} =this.state;
   
        return  (
            <View style={{backgroundColor:colors.textLight}}>
                {/* Loader */}
                 {loading ? <Loader/> : null} 

                <HomeIcon/>
        
            <ScrollView >
            <View  style={styles.container}>
            <View  style={styles.logoContainer}>
                
            <Image
                source={{uri : 'https://uttrition-uttrition-website.azurewebsites.net/images/logo_crop.jpg'}}
                style={styles.logo}
                resizeMode="contain"
            />
            </View>
            
         

            <Input
                label="Email"
                placeholder='Email'
                leftIcon={ <Icon  name='envelope-o' size={24}color={colors.mainColor} style={styles.inputIcon}/>}
                labelStyle={styles.labelStyle}
                containerStyle={styles.inputContainer}
                inputContainerStyle={styles.input}
                textContentType='emailAddress'
                value={email}
                onChangeText={(email) => this.setState({email})}
                //Next Button
                ref={ref => {this.email = ref;}} 
                returnKeyType = { "next" }
                onSubmitEditing={() => { this.password.focus(); }}
                
            />

            <Input
                label="Password"
                placeholder='Password'
                leftIcon={ <Icon name='lock' size={24}color={colors.mainColor} style={styles.inputIcon}/>}
                labelStyle={styles.labelStyle}
                containerStyle={styles.inputContainer}
                inputContainerStyle={styles.input}
                textContentType='password'
                secureTextEntry={true}
                value={password}
                onChangeText={(password) => this.setState({password})}

                //Next Button
                ref={ref => {this.password = ref;}} 
               
                
            />

      
            {errorMessage ?  <ErrorText message={errorMessage}/>: null}

            <Button
            title="Login"
            containerStyle={styles.buttonContainer}
            buttonStyle={styles.button}
            onPress={this.handleSubmit} 
            />


        <View style={styles.RegisterContainer}>
            <Text style={styles.createTextNormal}>Don't have an account ? </Text>
            <Text style={styles.createTextColored} onPress={()=> this.props.navigation.navigate('register')}>Register</Text>
        </View>


          </View>
          </ScrollView>
          </View>
        )
    }
}
// }

const styles = StyleSheet.create({
    container :
    {
        flex: 1,
        height: PHONE_HEIGHT,
        alignItems: 'center',  
        justifyContent:'center',
        backgroundColor : colors.textLight,
    },
    logoContainer:{
        alignSelf:'center',
        marginVertical:50, 
    },
    logo :
    {
         width: 100,
         height: 100 ,  
         
    },
    labelStyle:
    {
        marginLeft:10,
        
    },
    inputContainer :
    {
        width :PHONE_WIDTH*0.9,
        marginVertical:-7
        
    },
    inputIcon:
    {
        marginRight:5
    },
    input:
    {
       borderBottomWidth:0.5,
        borderWidth : 0.5,
        borderColor: colors.mainColor,
        borderRadius: 50,
        paddingHorizontal:15,
        marginTop:5,
        
    },
    buttonContainer:{
        
       
    },
    button:
    {
      backgroundColor: colors.mainColor,
      borderRadius:50,
      width :PHONE_WIDTH*0.9,
      paddingVertical:11

    },
    RegisterContainer:{
        flexDirection:'row',
        alignContent:'center',
        marginHorizontal: 40,
        marginTop:10,
        marginBottom:20
        
    },
    createTextNormal:{
        fontSize:15,
    },
    createTextColored:{
        fontSize:15,
        color:colors.mainColor,
    },
})

const mapStateToProps =state => {
    return {loginReducer : state.login}
}

export default connect(mapStateToProps, {login}) (LoginScreen);
// export default LoginScreen;